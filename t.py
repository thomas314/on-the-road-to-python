# Exercice 1.1
text ='Coucou les simploniens'
print(text)

# Exercice 1.2
nom = input('Entrez votre nom : ')
print("Bonjour", nom, "!")

# Exercice 1.3
# Écrire un programme qui demande à l'utilisateur la saisie de a et b et affiche la somme de a et de b.
a=input()
b=input()

print(a+b)

# Exercice 1.4
# Écrire un programme qui affiche une suite de 12 nombres dont chaque terme soit égal au triple du terme précédent.
n = 0
var = 1
result = []
while n != 12:
  if n == 0:
    result.append(var)

  else:
    var = var * 3
    result.append(var)

  n += 1

print(result)

# Exercice 1.5
for i in range(8):
    print(i*'*')

# Exercice 2
# Constituez une liste semaine contenant les 7 jours de la semaine. 
# À partir de cette liste, comment récupérez-vous seulement les 5 premiers jours de la semaine d’une part,
# et ceux du week-end d’autre part (utilisez pour cela l’indiçage) ? 
# Cherchez un autre moyen pour arriver au même résultat (en utilisant un autre indiçage).

semaine = ["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
print(semaine[:5])
print(semaine[5:])

print(semaine[:-2])
print(semaine[-2:])

semaine.reverse()
print(semaine)

hiver = ['janvier', 'février', 'mars']
automne = ['octobre', 'novembre', 'décembre']
été = ['juillet', 'août', 'septembre']
printemps = ['avril', 'mai', 'juin']
saisons = [hiver, printemps, été, automne]
print(saisons[2]) # -> ['juillet','aout', 'septembre' ]
print(saisons[1][0]) # -> avril
print(saisons[1:2]) # ->[['avril','mai','juin']]
print(saisons[:][1]) # -> ['avril','mai','juin']

# Comment expliquez-vous ce dernier résultat ?
# Le : appelle toutes les valeurs d'un tableau dans un tableau, ici il est demandé seulement le 1er tableau 



